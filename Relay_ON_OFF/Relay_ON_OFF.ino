/*
  IR Receiver Demonstration 3
  IR-Rcv-Demo3.ino
  Control LED's using Unused IR Remote keys

  DroneBot Workshop 2017
  http://dronebotworkshop.com
*/

// Include IR Remote Library by Ken Shirriff
#include <IRremote.h>

// Define sensor pin
const int RECV_PIN = 4;

// Define LED pin constants
const int Relay_Pin1 = 8; 
const int Relay_Pin2 = 9;

// Define integer to remember toggle state
int Relay1_togglestate = 0;
int Relay2_togglestate = 0;

// Define IR Receiver and Results Objects
IRrecv irrecv(RECV_PIN);
decode_results results;


void setup(){
  // Enable the IR Receiver
  irrecv.enableIRIn();
  // Set LED pins as Outputs
  pinMode(Relay_Pin1, OUTPUT);
  pinMode(Relay_Pin2, OUTPUT);
}


void loop(){
    if (irrecv.decode(&results)){

        switch(results.value){
          case 0x807F728D: // Keypad Button 1
          if(Relay1_togglestate==0){
          digitalWrite(Relay_Pin1, HIGH);
          Relay1_togglestate=1;
          }
          else {
          digitalWrite(Relay_Pin1, LOW);
          Relay1_togglestate=0;
          }
          
          case 0x807FB04F: // Keypad Button 2
          if(Relay2_togglestate==0){
          digitalWrite(Relay_Pin2, HIGH);
          Relay2_togglestate=1;
          }
          else {
          digitalWrite(Relay_Pin2, LOW);
          Relay2_togglestate=0;
          }
        break;
        
    }
    irrecv.resume(); 
  }

}
